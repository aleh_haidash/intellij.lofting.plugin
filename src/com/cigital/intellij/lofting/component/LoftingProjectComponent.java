package com.cigital.intellij.lofting.component;

import com.cigital.intellij.lofting.component.state.ProjectComponentState;
import com.cigital.intellij.lofting.entity.FileTimeStamp;
import com.cigital.intellij.lofting.listener.FileChangeListener;
import com.cigital.intellij.lofting.listener.FileOpenListener;
import com.intellij.openapi.components.*;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.messages.MessageBusConnection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * Created by haidash on 13.07.2015.
 */

@State(name = "LoftingProjectComponent", storages = @Storage(id = "LoftingProjectComponent", file = StoragePathMacros.PROJECT_CONFIG_DIR + "/Lofting.xml", scheme = StorageScheme.DIRECTORY_BASED))
public class LoftingProjectComponent implements ProjectComponent, PersistentStateComponent<ProjectComponentState> {
    /**
     * The LOGGER
     */
    private static final Logger LOGGER = Logger.getInstance(LoftingProjectComponent.class.getName());

    /**
     * The message bus connection
     */
    private final MessageBusConnection msgBus;

    /**
     * The project
     */
    private final Project project;

    /**
     * The project component state
     */
    private ProjectComponentState componentState;

    public LoftingProjectComponent(final Project project) {
        this.project = project;
        this.componentState = new ProjectComponentState();

        final MessageBus messageBus = project.getMessageBus();
        this.msgBus = messageBus.connect(project);
    }

    @Override
    public void projectOpened() {
        msgBus.subscribe(VirtualFileManager.VFS_CHANGES, new FileChangeListener(project));
        msgBus.subscribe(FileEditorManagerListener.FILE_EDITOR_MANAGER, new FileOpenListener(project));
    }

    @Override
    public void projectClosed() {

    }

    @Override
    public void initComponent() {
    }

    @Override
    public void disposeComponent() {
        msgBus.disconnect();
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "LoftingProjectComponent";
    }

    @Nullable
    @Override
    public ProjectComponentState getState() {
        return componentState;
    }

    @Override
    public void loadState(ProjectComponentState projectComponentState) {
        this.componentState = projectComponentState;
    }

    public void saveTimeStamp(@NotNull final String path, final long timeStamp) {
        final FileTimeStamp fileTimeStamp = new FileTimeStamp(path, timeStamp);
        final Map<String, FileTimeStamp> timeStamps = componentState.getTimeStamps();
        timeStamps.put(path, fileTimeStamp);
    }

    public Map<String, FileTimeStamp> getTimeStamps() {
        return componentState.getTimeStamps();
    }
}
