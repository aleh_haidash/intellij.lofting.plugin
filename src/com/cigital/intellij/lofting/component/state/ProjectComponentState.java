package com.cigital.intellij.lofting.component.state;

import com.cigital.intellij.lofting.entity.FileTimeStamp;
import com.intellij.util.xmlb.annotations.Property;

import java.util.HashMap;
import java.util.Map;

/**
 * Description of the provider state project level.
 * <p>
 * Created by haidash on 13.07.2015.
 */
public class ProjectComponentState {

    /**
     * The time stamps map.
     */
    @Property
    private Map<String, FileTimeStamp> timeStamps;

    /**
     * The constructor ProjectComponentState
     */
    public ProjectComponentState() {
        this.timeStamps = new HashMap<>();
    }

    /**
     * @return the time stamps map.
     */
    public Map<String, FileTimeStamp> getTimeStamps() {
        return timeStamps;
    }
}
