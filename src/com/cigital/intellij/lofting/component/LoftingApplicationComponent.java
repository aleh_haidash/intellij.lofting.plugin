package com.cigital.intellij.lofting.component;

import com.cigital.intellij.lofting.component.state.ApplicationComponentState;
import com.intellij.openapi.components.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by haidash on 13.07.2015.
 */

@State(name = "LoftingAppComponent", storages = @Storage(id = "LoftingAppComponent", file = StoragePathMacros.APP_CONFIG + "/lofting_app.xml", scheme = StorageScheme.DIRECTORY_BASED))
public class LoftingApplicationComponent implements ApplicationComponent, PersistentStateComponent<ApplicationComponentState> {

    /**
     * The current state of this provider
     */
    private ApplicationComponentState componentState;

    public LoftingApplicationComponent() {
        this.componentState = new ApplicationComponentState();

    }

    @Override
    public void initComponent() {
    }

    @Override
    public void disposeComponent() {

    }

    @NotNull
    @Override
    public String getComponentName() {
        return "LoftingApplicationComponent";
    }

    @Nullable
    @Override
    public ApplicationComponentState getState() {
        return componentState;
    }

    @Override
    public void loadState(ApplicationComponentState applicationComponentState) {
        this.componentState = applicationComponentState;
    }
}
