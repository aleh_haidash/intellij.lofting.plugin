package com.cigital.intellij.lofting.service.impl;

import com.cigital.intellij.lofting.component.LoftingProjectComponent;
import com.cigital.intellij.lofting.entity.FileTimeStamp;
import com.cigital.intellij.lofting.service.CommunicationService;
import com.intellij.notification.Notification;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static com.intellij.notification.NotificationType.ERROR;
import static com.intellij.notification.NotificationType.INFORMATION;
import static com.intellij.notification.Notifications.Bus;

/**
 * Created by haidash on 13.07.2015.
 */
public class CommunicationServiceImpl implements CommunicationService {

    /**
     * The LOGGER
     */
    private static final Logger LOGGER = Logger.getInstance(CommunicationService.class.getName());
    public static final String LOFTING_PLUGIN_ID = "lofting.plugin";
    public static final String LOFTING_PLUGIN_TITLE = "Lofting plugin";
    public static final String ERROR_HAS_BEEN_EXCEEDED_FILE_SIZE_LIMIT = "Error has been exceeded file size limit";
    public static final String FILE_IS_NOT_EXIST = "File %fileName% is not exist.";

    @Override
    public String getHtmlContent() {

        final HttpGet get = new HttpGet("http://localhost:8080/get_content");
        get.addHeader(HEADER_ACCEPT, "application/xml");
        get.addHeader(HEADER_CONNECTION, "keep-alive");
        get.addHeader(HEADER_X_REQUESTED_BY, "true");

        final CloseableHttpClient httpClient = HttpClients.createMinimal();
        try {

            final CloseableHttpResponse response = httpClient.execute(get);
            final StatusLine statusLine = response.getStatusLine();

            if (statusLine.getStatusCode() != 200) {
                throw new RuntimeException(statusLine.toString());
            }

            final HttpEntity entity = response.getEntity();

            return IOUtils.toString(entity.getContent(), "UTF-8");

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private boolean needSendContent(final String relativePath, final long timeStamp, final Project project) {

        final LoftingProjectComponent component = project.getComponent(LoftingProjectComponent.class);
        final Map<String, FileTimeStamp> timeStamps = component.getTimeStamps();

        final FileTimeStamp fileTimeStamp = timeStamps.get(relativePath);

        if (fileTimeStamp == null) {
            return true;
        }

        final long prevTimeStamp = fileTimeStamp.getTimeStamp();

        return prevTimeStamp < timeStamp;

    }

    public void sendFile(@NotNull final PsiFile psiFile, final Project project) {

        final String fileName = psiFile.getName();
        final VirtualFile virtualFile = psiFile.getVirtualFile();

        final Application application = ApplicationManager.getApplication();

        if (virtualFile == null || !virtualFile.exists()) {

            application.invokeLater(() ->
                    Bus.notify(new Notification(LOFTING_PLUGIN_ID, LOFTING_PLUGIN_TITLE, FILE_IS_NOT_EXIST.replace("%fileName%", fileName), ERROR)));

            return;
        }

        final String basePath = project.getBasePath();
        final Path filePath = Paths.get(virtualFile.getPath());
        final String relativePath = Paths.get(basePath).relativize(filePath).toString();
        final long timeStamp = virtualFile.getTimeStamp();

        if (!needSendContent(relativePath, timeStamp, project)) {
            return;
        }

        final HttpPost post = new HttpPost("http://localhost:8080/upload_file");
        post.addHeader(HEADER_CONNECTION, "keep-alive");
        post.addHeader(HEADER_X_REQUESTED_BY, "true");

        final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("file", new FileBody(filePath.toFile()));
        builder.addTextBody("path", relativePath);

        post.setEntity(builder.build());

        try (final CloseableHttpClient httpClient = HttpClients.createMinimal()) {

            final CloseableHttpResponse response = httpClient.execute(post);
            final StatusLine statusLine = response.getStatusLine();

            final HttpEntity entity = response.getEntity();

            if (statusLine.getStatusCode() != 200 && entity.getContentLength() == 0) {

                application.invokeLater(() ->
                        Bus.notify(new Notification(LOFTING_PLUGIN_ID, LOFTING_PLUGIN_TITLE, ERROR_HAS_BEEN_EXCEEDED_FILE_SIZE_LIMIT, ERROR)));

                return;
            }

            if (statusLine.getStatusCode() != 200) {
                throw new RuntimeException(statusLine.toString());
            }

            final LoftingProjectComponent component = project.getComponent(LoftingProjectComponent.class);
            component.saveTimeStamp(relativePath,timeStamp);

            String responseContent;
            try {
                responseContent = IOUtils.toString(entity.getContent());
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }

            if (responseContent == null || responseContent.isEmpty()) {
                return;
            }

            application.invokeLater(() ->
                    Bus.notify(new Notification(LOFTING_PLUGIN_ID, LOFTING_PLUGIN_TITLE, responseContent, INFORMATION)));

        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }


}
