package com.cigital.intellij.lofting.service;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

/**
 * Created by haidash on 13.07.2015.
 */
public interface CommunicationService {

    /**
     * The Constant HEADER_ACCEPT
     */
    public String HEADER_ACCEPT = "Accept";

    /**
     * The Constant HEADER_CONNECTION
     */
    public String HEADER_CONNECTION = "Connection";

    /**
     * The Constant HEADER_X_REQUESTED_BY
     */
    public String HEADER_X_REQUESTED_BY = "X-Requested-By";

    /**
     * The Constant HEADER_CONTENT_TYPE
     */
    public String HEADER_CONTENT_TYPE = "Content-Type";

    /**
     * @return html5 content with canvas
     */
    public String getHtmlContent();

    /**
     * Send file
     *
     * @param psiFile psi file
     * @param project project
     */
    public void sendFile(@NotNull final PsiFile psiFile, final Project project);
}
