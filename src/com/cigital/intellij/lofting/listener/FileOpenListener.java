package com.cigital.intellij.lofting.listener;

import com.cigital.intellij.lofting.service.CommunicationService;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerAdapter;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.components.ServiceManager.getService;

/**
 * The Class FileOpenListener
 * <p>
 * Created by haidash on 14.07.2015.
 */
public class FileOpenListener extends FileEditorManagerAdapter {

    /**
     * The project
     */
    private final Project project;

    /**
     * The psi manager
     */
    private final PsiManager psiManager;

    /**
     * The constructor FileOpenListener
     *
     * @param project the project
     */
    public FileOpenListener(Project project) {
        this.project = project;
        this.psiManager = PsiManager.getInstance(project);
    }

    @Override
    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file) {

        if (!file.exists()) {
            return;
        }

        final PsiFile psiFile = psiManager.findFile(file);

        if (psiFile == null || StringUtil.endsWith(psiFile.getName(), ".xml")) {
            return;
        }

        final CommunicationService statisticService = getService(CommunicationService.class);
        statisticService.sendFile(psiFile, project);
    }
}

