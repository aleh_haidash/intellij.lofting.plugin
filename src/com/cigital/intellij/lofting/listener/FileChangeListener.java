package com.cigital.intellij.lofting.listener;

import com.cigital.intellij.lofting.service.CommunicationService;
import com.intellij.history.core.LocalHistoryFacade;
import com.intellij.history.core.tree.RootEntry;
import com.intellij.history.integration.IdeaGateway;
import com.intellij.history.integration.LocalHistoryImpl;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import com.intellij.openapi.vfs.newvfs.events.VFileEvent;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.intellij.openapi.components.ServiceManager.getService;

/**
 * The Class FileChangeListener
 * <p>
 * Created by haidash on 13.07.2015.
 */
public class FileChangeListener extends BulkFileListener.Adapter {

    /**
     * The project
     */
    private final Project project;

    /**
     * The root
     */
    private final RootEntry root;

    /**
     * The facade
     */
    private final LocalHistoryFacade facade;

    /**
     * The constructor FileChangeListener
     *
     * @param project the project
     */
    public FileChangeListener(Project project) {
        this.project = project;

        final LocalHistoryImpl instanceImpl = LocalHistoryImpl.getInstanceImpl();
        final IdeaGateway gateway = instanceImpl.getGateway();

        this.root = gateway.createTransientRootEntry();
        this.facade = instanceImpl.getFacade();
    }

    @Override
    public void after(@NotNull List<? extends VFileEvent> events) {

        final PsiManager psiManager = PsiManager.getInstance(project);
        final CommunicationService statisticService = getService(CommunicationService.class);

        for (final VFileEvent event : events) {

            final VirtualFile file = event.getFile();

            if (file == null || !file.exists()) {
                continue;
            }

            final PsiFile psiFile = psiManager.findFile(file);

            if (psiFile == null || StringUtil.endsWith(psiFile.getName(), ".xml")) {
                continue;
            }

            statisticService.sendFile(psiFile, project);
        }
    }
}
