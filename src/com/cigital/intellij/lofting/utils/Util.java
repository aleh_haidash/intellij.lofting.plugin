package com.cigital.intellij.lofting.utils;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The class Util
 * <p>
 * Created by haidash on 14.07.2015.
 */
public class Util {

    public static String getProjectRelativePath(final PsiFile psiFile) {

        final VirtualFile virtualFile = psiFile.getVirtualFile();
        final Project project = psiFile.getProject();

        final String basePath = project.getBasePath();
        final String path = virtualFile.getPath();

        final Path relative = Paths.get(basePath).relativize(Paths.get(path));

        return relative.toString();
    }

    public static String getFileContent(VirtualFile virtualFile) {

        final FileDocumentManager documentManager = FileDocumentManager.getInstance();
        final Document document = documentManager.getDocument(virtualFile);

        if (document == null) {
            throw null;
        }

        return document.getText();
    }

    public static String getContent(final Project project, final PsiFile file, final long timeStamp) {

        final VirtualFile virtualFile = file.getVirtualFile();
        final String path = virtualFile.getPath();

        final FileDocumentManager documentManager = FileDocumentManager.getInstance();
        final Document document = documentManager.getDocument(virtualFile);

        if (document == null) {
            return null;
        }

        final String contentFirst = document.getText();


        return contentFirst;

        //TODO need implement logic
//        final LocalHistoryImpl instanceImpl = LocalHistoryImpl.getInstanceImpl();
//        final IdeaGateway gateway = instanceImpl.getGateway();
//        final RootEntry root = gateway.createTransientRootEntry();
//        final LocalHistoryFacade facade = instanceImpl.getFacade();
//
//        if (timeStamp == -1) {
//            return contentFirst;
//        }
//
//        final RevisionsCollector collector = new RevisionsCollector(facade, root, path, project.getLocationHash(), null);
//        final List all = collector.getResult();
//
//        String contentSecond = null;
//
//        for (Object o : all) {
//            if (!(o instanceof Revision)) {
//                continue;
//            }
//
//            final Revision revision = (Revision) o;
//
//            if (revision.getTimestamp() == timeStamp) {
//                final Entry entry = revision.findEntry();
//                final Content content = entry.getContent();
//
//                contentSecond = content.toString();
//
//                break;
//            }
//        }
//
//        if (contentSecond == null) {
//            return contentFirst;
//        }
//
//        final List<LineFragment> lineFragments = new ArrayList<>();
//
//        final TextCompareProcessor processor = new TextCompareProcessor(ComparisonPolicy.IGNORE_SPACE, DiffPolicy.DEFAULT_LINES, HighlightMode.BY_LINE);
//
//        try {
//            lineFragments.addAll(processor.process(contentFirst, contentSecond));
//        } catch (FilesTooBigForDiffException e) {
//            e.printStackTrace();
//        }
//
//        return "";
    }
}
