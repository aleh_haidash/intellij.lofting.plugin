package com.cigital.intellij.lofting.ui.panel;

import com.cigital.intellij.lofting.service.CommunicationService;
import com.cigital.intellij.lofting.ui.browser.SwingFXWebView;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.components.JBPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.intellij.openapi.components.ServiceManager.getService;

/**
 * The Class GuidancePanel
 * <p>
 * Created by shelden93 on 01.06.2015.
 */
public class GuidancePanel extends JBPanel {

    /**
     * The project
     */
    private final Project project;

    /**
     * The constructor.
     *
     * @param parent  The parent.
     * @param project The project.
     */
    public GuidancePanel(final ToolWindow parent, final Project project) {
        this.project = project;

        setLayout(new GridBagLayout());

        SwingFXWebView webView = new SwingFXWebView();

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.weighty = 0.5;
        constraints.weightx = 0.5;

        add(webView, constraints);

        final JButton refreshButton = new JButton("Refresh");
        refreshButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(final MouseEvent e) {
                final CommunicationService communicationService = getService(CommunicationService.class);
                final String htmlContent = communicationService.getHtmlContent();
                webView.showHtmlContent(htmlContent);
            }
        });

        constraints = new GridBagConstraints();
        constraints.insets = new Insets(0, 5, 0, 0);
        constraints.gridx = 0;
        constraints.gridy = 0;

        add(refreshButton, constraints);
    }
}
