package com.cigital.intellij.lofting.ui.panel;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;

import javax.swing.*;

/**
 * The Class GuidancePanelFactory
 * <p>
 * Created by shelden93 on 01.06.2015.
 */
public class GuidancePanelFactory implements ToolWindowFactory {

    /**
     * (non-Javadoc)
     *
     * @see ToolWindowFactory#createToolWindowContent(Project, ToolWindow)
     */
    @Override
    public void createToolWindowContent(final Project project, final ToolWindow toolWindow) {
        final JComponent component = toolWindow.getComponent();
        component.add(new GuidancePanel(toolWindow, project));
    }
}
