package com.cigital.intellij.lofting.ui.browser;

import com.intellij.openapi.util.text.StringUtil;
import com.intellij.ui.components.JBPanel;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;

import java.awt.*;

/**
 * The Class SwingFXWebView
 * <p/>
 * Created by shelden93 on 16.06.2015.
 */
public class SwingFXWebView extends JBPanel {

    /**
     * The web view
     */
    private WebView webView;

    /**
     * The constructor SwingFXWebView
     */
    public SwingFXWebView() {
        setLayout(new BorderLayout());

        final JFXPanel jfxPanel = new JFXPanel();

        add(jfxPanel, BorderLayout.CENTER);

        Platform.setImplicitExit(false);
        Platform.runLater(() -> createScene(jfxPanel));
    }

    /**
     * Create java fx scene and web view
     *
     * @param fxPanel - The fx panel
     */
    private void createScene(final JFXPanel fxPanel) {
        webView = new WebView();
        webView.setContextMenuEnabled(false);
        fxPanel.setScene(new Scene(webView));
    }

    /**
     * Show content in web view
     *
     * @param content - The content
     */
    public void showHtmlContent(final String content) {
        if (StringUtil.isEmpty(content)) {
            return;
        }
        Platform.runLater(() -> webView.getEngine().loadContent(content));
    }

    /**
     * Add location listener
     *
     * @param listener the listener
     */
    public void addLocationListener(final ChangeListener listener) {
        Platform.runLater(() -> {
            final WebEngine webEngine = webView.getEngine();
            final ReadOnlyStringProperty locationProperty = webEngine.locationProperty();
            locationProperty.addListener(listener);
        });
    }

    /**
     * @return if string null was last page
     */
    public String goBack() {

        final WebEngine webEngine = webView.getEngine();
        final WebHistory history = webEngine.getHistory();

        final ObservableList<WebHistory.Entry> entryList = history.getEntries();
        int currentIndex = history.getCurrentIndex();


        Platform.runLater(() -> {
            if (currentIndex > 0) {
                history.go(-1);
            } else {
                history.setMaxSize(0);
                history.setMaxSize(100);
            }
        });

        if (currentIndex > 0) {
            return entryList.get(currentIndex - 1).getUrl();
        }

        return null;
    }
}
