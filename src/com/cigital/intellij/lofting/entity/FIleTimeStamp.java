package com.cigital.intellij.lofting.entity;

import java.io.Serializable;

/**
 * Created by haidash on 15.07.2015.
 */
public class FileTimeStamp implements Serializable {

    private long timeStamp;
    private String path;

    public FileTimeStamp(final String path, final long timeStamp) {
        this.timeStamp = timeStamp;
        this.path = path;
    }

    public FileTimeStamp() {
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileTimeStamp that = (FileTimeStamp) o;

        if (timeStamp != that.timeStamp) return false;

        return path.equals(that.path);

    }

    @Override
    public int hashCode() {

        int result = (int) (timeStamp ^ (timeStamp >>> 32));
        result = 31 * result + path.hashCode();

        return result;
    }

    @Override
    public String toString() {
        return "FileTimeStamp [path=\"" + path + "\", timeStamp=" + timeStamp
                + "]";
    }
}
